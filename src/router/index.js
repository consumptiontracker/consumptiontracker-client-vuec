import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from '../views/LoginView.vue'
import GaugeView from '../views/GaugeView.vue'
import HistoryView from '../views/HistoryView.vue'
import AboutView from '../views/AboutView.vue'
import ErrorView from '../views/ErrorView'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/login',
    name: 'login',
    component: LoginView
  },
  {
    path: '/history',
    name: 'history',
    component: HistoryView
  },
  {
    path: '/gauges',
    name: 'gauges',
    component: GaugeView
  },
  {
    path: '/about',
    name: 'about',
    component: AboutView
  },
  {
    path: '/error',
    name: 'error',
    component: ErrorView
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
