import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './assets/global.css'
import { createStore } from 'vuex'
import FetchApi from './util/FetchApi'
import Auth from './util/Auth'
import StatsContainer from './model/StatsContainer'
import Util, { ERROR_UNAUTHORIZED, MESSAGE_TIMEOUT } from './util/Util'

const store = createStore({
  state() {
    return {
      error: null,
      message: null,
      username: Auth.getUsername(),
      gauges: [],
      measurements: [],
      stats: null,
    }
  },
  mutations: {
    setError(state, payload) {
      state.error = payload
    },
    setMessage(state, payload) {
      state.message = payload
    },
    setUsername(state, payload) {
      state.username = payload
    },
    setGauges(state, payload) {
      state.gauges = payload
    },
    setMeasurements(state, payload) {
      state.measurements = payload
    },
    setStats(state, payload) {
      state.stats = payload
    },
  },
  actions: {
    clearError(context) {
      context.commit('setError', null)
    },
    setError(context, payload) {
      context.commit('setError', payload)
    },
    displayMessage(context, payload) {
      context.commit('setMessage', payload)
      setTimeout(() => context.commit('setMessage', null), MESSAGE_TIMEOUT)
    },
    logout(context) {
      Auth.clearAuthentication()
      context.commit('setUsername', null)
      context.commit('setGauges', null)
      context.commit('setMeasurements', null)
      context.commit('setStats', null)
    },
    async login(context, payload) {
      try {
        context.commit('setError', null)
        const session = await FetchApi.login(payload)
        Auth.storeAuthentication(session)
        context.commit('setUsername', Auth.getUsername())
      } catch (e) {
        console.error('ERROR!', e)
        if (e.message === ERROR_UNAUTHORIZED) {
          context.commit('setError', 'Invalid credentials!')
        } else {
          context.commit('setError', 'Connection failure!')
        }
      }
    },
    async fetchConsumptionData(context) {
      try {
        context.commit('setError', null)
        const [gauges, measurements] = await Promise.all([
          FetchApi.getGauges(),
          FetchApi.getMeasurements(),
        ])
        Util.adjustMeasurements(gauges, measurements)
        const stats = new StatsContainer(measurements)
        context.commit('setGauges', gauges)
        context.commit('setMeasurements', measurements)
        context.commit('setStats', stats)
      } catch (e) {
        console.error('ERROR!', e)
        if (e.message === ERROR_UNAUTHORIZED) {
          context.commit('setError', 'Session expired!')
          context.dispatch('logout')
        } else {
          context.commit('setError', 'Connection failure!')
        }
      }
    },
    async postConsumptionData(context, payload) {
      try {
        context.commit('setError', null)
        if (payload.length > 0) {
          await FetchApi.postMeasurements(payload)
          context.dispatch('displayMessage', 'Successfully saved!')
          context.dispatch('fetchConsumptionData')
        }
      } catch (e) {
        console.error('ERROR!', e)
        if (e.message === ERROR_UNAUTHORIZED) {
          context.commit('setError', 'Session expired!')
          context.dispatch('logout')
        } else {
          context.commit('setError', 'Connection failure!')
        }
      }
    },
  },
  getters: {
    error: (state) => state.error,
    loginUsername: (state) => state.username,
    consumptionDataLoaded: (state) => !!state.stats,
  },
})

createApp(App).use(router).use(store).mount('#app')
