function currentMonthReportingDate() {
  const date = new Date()
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  return generateReportingDateStr(`${year}-${month}-1`)
}

function previousReportingDate(dateStr) {
  if (!dateStr) {
    return null
  }
  const [year, month] = dateStr.split('-')
  const repMonth = decreaseMonth(month)
  const repYear = decreaseYearIfNeeded(month, year)
  return generateReportingDateStr(`${repYear}-${repMonth}-1`)
}

function nextReportingDate(dateStr) {
  if (!dateStr) {
    return null
  }
  const [year, month] = dateStr.split('-')
  const repMonth = incrementMonth(month)
  const repYear = incrementYearIfNeeded(month, year)
  return generateReportingDateStr(`${repYear}-${repMonth}-1`)
}

const incrementMonth = (month) => (+month === 12 ? 1 : ++month)
const incrementYearIfNeeded = (month, year) => (+month == 12 ? ++year : year)
const decreaseMonth = (month) => (+month === 1 ? 12 : --month)
const decreaseYearIfNeeded = (month, year) => (+month === 1 ? --year : year)
const isLeapYear = (year) =>
  (+year % 4 === 0 && +year % 100 !== 0) || +year % 400 === 0
function generateReportingDateStr(dateStr) {
  if (!dateStr) {
    return null
  }
  const [year, month] = dateStr.split('-')
  const day = getLastDayOfMonth(year, month)
  return `${year}-${month.padStart(2, '0')}-${day}`
}

function getLastDayOfMonth(year, month) {
  if (!year || !month) {
    return 1
  }
  switch (+month) {
    case 1:
      return 31
    case 2:
      return isLeapYear(year) ? 29 : 28
    case 3:
      return 31
    case 4:
      return 30
    case 5:
      return 31
    case 6:
      return 30
    case 7:
      return 31
    case 8:
      return 31
    case 9:
      return 30
    case 10:
      return 31
    case 11:
      return 30
    case 12:
      return 31
  }
}

export default {
  currentMonthReportingDate,
  previousReportingDate,
  nextReportingDate,
}
