import Auth from './Auth'

// Constants
export const ERROR_UNAUTHORIZED = 'Unauthorized'
export const ERROR_FAILED_TO_FETCH = 'Failed to fetch'
export const MESSAGE_TIMEOUT = 1000 // in millis

// Response and error handlers

function handleResponse(response) {
  if (response.status >= 401 && response.status < 500) {
    Auth.clearAuthentication()
    throw new Error('Unauthorized')
  }
}

async function handleJsonResponse(response) {
  if (response.status >= 401 && response.status < 500) {
    Auth.clearAuthentication()
    throw new Error('Unauthorized')
  }
  return await response.json()
}

function handleError(e, unauthorizedCallback) {
  console.log('Error occurred', e)
  if (e.message === 'Unauthorized') {
    unauthorizedCallback()
  }
}

function handleRoutingOnError(e, router) {
  console.log('ERROR', e)
  if (e.message === ERROR_UNAUTHORIZED) {
    router.push({ name: 'login' })
  } else {
    router.push({ name: 'error' })
  }
}

// Consumption processing functions

function getMeasurement(recordingDate, gauge, allMeasurements) {
  if (!recordingDate || !gauge || !allMeasurements) {
    return null
  }
  return allMeasurements
    .filter((measurement) => measurement.gaugeId === gauge.id)
    .find((measurement) => measurement.recordingDate === recordingDate)
}

function getPreviousMeasurement(currentMeasurement, allMeasurements) {
  return getPreviousMeasurementForRecordingDate(
    currentMeasurement.recordingDate,
    currentMeasurement.gauge,
    allMeasurements
  )
}

function getPreviousMeasurementForRecordingDate(
  recordingDate,
  gauge,
  allMeasurements
) {
  return allMeasurements?.find(
    (measurement) =>
      measurement.recordingDate < recordingDate &&
      measurement.gauge.type == gauge.type &&
      measurement.gauge.name == gauge.name
  )
}

function getConsumption(measurement, previousMeasurement) {
  const currentMeasurementDelta = measurement.value - measurement.offset
  const previousMeasurementDelta = previousMeasurement
    ? previousMeasurement.value - previousMeasurement.offset
    : 0
  return currentMeasurementDelta - previousMeasurementDelta
}

function adjustMeasurements(gauges, measurements) {
  measurements.forEach(
    (measurement) =>
      (measurement.gauge = gauges.find(
        (gauge) => measurement.gaugeId === gauge.id
      ))
  )
  measurements.forEach((measurement) => {
    const previousMeasurement = getPreviousMeasurement(
      measurement,
      measurements
    )
    measurement.consumption = getConsumption(measurement, previousMeasurement)
  })
}

// Math functions

function round(value) {
  return value ? Math.round(value * 100) / 100 : value
}

// String functions

function formatUnit(unit) {
  if (unit === 'm3') {
    return 'm<sup>3</sup>'
  } else {
    return unit
  }
}

export default {
  handleResponse,
  handleJsonResponse,
  handleError,
  handleRoutingOnError,
  getMeasurement,
  getPreviousMeasurement,
  getPreviousMeasurementForRecordingDate,
  getConsumption,
  adjustMeasurements,
  round,
  formatUnit,
}
